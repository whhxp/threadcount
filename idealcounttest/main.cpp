#include <QCoreApplication>
#include <QThread>
#include <QDebug>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug()<<"The logical core number is "<<QThread::idealThreadCount();
    return a.exec();
}
